<?php
// $Id$

/**
 * @file
 * Administrative page callbacks for the nimbits module.
 */

/**
 * Implementation of hook_admin_settings() for configuring the module
 */
function nimbits_admin_settings_form(&$form_state) {
  
  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('Nimbits server general settings'),
    '#collapsible' => FALSE,
  );

  $form['general']['nimbits_server_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Nimbits server location'),
    '#default_value' => variable_get('nimbits_server_url', ''),
    '#size' => 30,
    '#maxlength' => 80,
    '#required' => TRUE,
    '#description' => t('The web-adres of the Nimbits server you would like to connect with. (The public Nimbits server is <a href="@nimbits">http://app.nimbits.com/service</a>)', array('@nimbits' => 'http://app.nimbits.com/service')),
  );
  
  $form['general']['nimbits_server_mail'] = array(
    '#type' => 'textfield',
    '#title' => t('Nimbits server email'),
    '#default_value' => variable_get('nimbits_server_mail', ''),
    '#size' => 30,
    '#maxlength' => 80,
    '#required' => TRUE,
    '#description' => t('The Nimbits server email adres for public access'),
  );
  
  $form['general']['nimbits_server_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Nimbits server key'),
    '#default_value' => variable_get('nimbits_server_key', ''),
    '#size' => 30,
    '#maxlength' => 80,
    '#required' => FALSE,
    '#description' => t('A server secret that governs the entire system. The Server Secret is configured by the owner of the entire Nimbits instance. That is, the secret configured by the owner during the building of an entire nimbits server.  If this secret is provided, no other authentication is needed.'),
  );
  
  $form['user'] = array(
    '#type' => 'fieldset',
    '#title' => t('Nimbits user settings'),
    '#collapsible' => FALSE,
  );
  $form['user']['nimbits_user_mail'] = array(
    '#type' => 'textfield',
    '#title' => t('Nimbits user email'),
    '#default_value' => variable_get('nimbits_user_mail', ''),
    '#size' => 30,
    '#maxlength' => 80,
    '#required' => FALSE,
    '#description' => t('The Nimbits user email adres for private access'),
  );
  $form['user']['nimbits_user_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Nimbits user key'),
    '#default_value' => variable_get('nimbits_user_key', ''),
    '#size' => 30,
    '#maxlength' => 80,
    '#required' => FALSE,
    '#description' => t('a user secret specific to each email address in the user database on a particular 
Nimbits Server. When the secret parameter is provided, the server will check if the provided secret matches either the secret connected to the provided email or the server secret itself. Users can create and reset their secret key by logging into a Nimbits Server and clicking the Secret Key Icon on the main menu. '),
  );
  
  // Output a positive status message, since users keep on asking whether
  // Nimbits should work or not. Re-check on every regular visit of this form to
  // verify the module's configuration.

  $status = _nimbits_status(true);
  
  if (empty($form_state['post'])) {
    // If there is any configuration error, then mollom_init() will have output
    // it already.
    if ($status === TRUE) {
      //test
      nimbits_get_points(17); 
      
      drupal_set_message(t('Nimbits server verified your key. The service is operating correctly.'));
    }
    else {
      drupal_set_message(t('There are troubles with Nimbits. Please check the credentials. '), 'error');
    }
  }


  return system_settings_form($form);
}